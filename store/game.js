
// импортируем зарос graphql
import gameInfo from '@/graph/query/gameInfo';
import trophyList from '@/graph/query/trophyList';

export const state = () => ({
    gameInfo: {},
    /** Идентификатор активной игры */
    activeId: '',
    /** параметры запроса graphql */
    params: {
        radioButton: 'trophies',
        dropdown: '',
    },
    /** список dlc (включает осоновную игру) */
    dlcList: {},

    /** Список трофеев */
    trophyList: {},

    /** Объект с переводом для страницы */
    translate: {
        radioButton: {
            trophies: 'Трофеи',
            guide: 'Гайды',
            feedback: 'Отзывы',
        },
        dropdown: { },
    },
  })
  
  export const getters = {
    /** Возвращает информацию об игре */
    getGameInfo(state){
        return state.gameInfo;
    },

    /** Возвращает параметры по умолчанию */
    getParams(state) {
        return state.params
    },

    /** Возвращает перевод для radio button */
    getTextRadioButton(state){
        return state.translate.radioButton;
    },

    /** Возвращает перевод для dropdown */
    getTextDropdown(state){
        return state.translate.dropdown
    },

    /** Возвращает список dlc включая главную игру */
    getDLCList(state){
        return state.dlcList;
    },
    /** Возвращает список трофеев */
    getTrophyList(state){
        return state.trophyList;
    },
  }
  
  export const mutations = {
    /** Записывает пришедшую с сервера(graphql) информацию о игре */
    setGameInfo(state, gameInfo){
        state.gameInfo = gameInfo;
    },

    /** Записывает в стор идентификатор активной игры */
    setActiveGameId(state, gameId){
        state.activeId = gameId;
        
    },
    /** Записывает в стор список dlc. преобразует в нужный вид */
    setDLCList(state, dlcList){
        let result = {};
        for(let i = 0; i < dlcList.length; ++i){
            result[dlcList[i]._id] = dlcList[i].gameName;
        }
        if(!dlcList.length || !(state.activeId in result)){
            result = Object.assign({[state.activeId]: state.gameInfo.gameInfo.gameName}, result);
        }
        state.dlcList = result;
    },
    /** Устанавливает значение для dropdown */
    setActiveDropdown(state, gameId){
        // console.log('state==> ', state);
        state.params.dropdown = gameId;

    },

    /** Устанавливает значение для radiobutton */
    setActiveRadioButton(state, typepage){
        state.params.radioButton = state.translate.radioButton[typepage];
    },

    /** Записывает список трофеев для игры */
    setTrophyList(state, trophyList){
        state.trophyList = trophyList;
    }
  }
  
  export const actions = {
    /** Загрузчик общей информации об игре с сервера. Инициируетя хуком fetch */
    async fetchGameInfo({ state, commit, getters, rootGetters, rootState },  gameId){
        // console.log('fetchGameInfo typepage ==> ', typepage);
        // console.log('fetchGameInfo gameId ==> ', gameId);
        try{

            // хранится ли переданный id в сторе, в списке игр для dropdown
           let equally = gameId in getters.getDLCList;
        //    console.log('equally ==> ', equally);
            let result = await this.app.apolloProvider.clients.defaultClient.query({
                query: gameInfo, 
                variables: {
                    lang: rootState.lang,  // берем из корневого стора
                    id: gameId,
                    void: equally,
                },
            });
            console.log('fetchGameInfo ==>', result.data.gameInfo);
            // TODO Проверить на ошибки!!!!!!
            // записываем пришедший результат
            commit('setGameInfo', result.data.gameInfo);
            // устанавливаем id активной игры
            commit('setActiveGameId', gameId);

            // если это новая игра, то обновляем список dlc
            if(!equally){
                commit('setDLCList', result.data.gameInfo.gameDLCList)
            }

            commit('setActiveDropdown', gameId);
        }catch(err){
            console.log('Ошибка в сторе game.js > экшн fetchGameInfo ==> ', err);
        }
    },

    /** Загрузчик информации о списке трофеев для игры */
    async fetchTrophyList({ state, commit, getters, rootGetters, rootState }, gameId){
        try{
            let result = await this.app.apolloProvider.clients.defaultClient.query({
                query: trophyList, 
                variables: {
                    lang: rootState.lang,  // берем из корневого стора
                    id: gameId,
                },
            });
            // console.log('fetchTrophyList ==> ', result);
            commit('setTrophyList', result.data.trophyList);
            // записываем активную страницу
            commit('setActiveRadioButton', 'trophies');
        }catch(err){
            console.log('Ошибка в сторе game.js > экшн fetchTrophyList ==> ', err);
        }
    },
    async fetchGuideList({ state, commit, getters, rootGetters, rootState }, gameId){
        try{
            
           
            commit('setActiveRadioButton', 'guide');
        }catch(err){
            console.log('Ошибка в сторе game.js > экшн fetchGuideList ==> ', err);
        }
    },
    async fetchFeedbackList({ state, commit, getters, rootGetters, rootState }, gameId){
        try{
            
            // записываем активную страницу
            commit('setActiveRadioButton', 'feedback');
        }catch(err){
            console.log('Ошибка в сторе game.js > экшн fetchFeedbackList ==> ', err);
        }
    },
  }
