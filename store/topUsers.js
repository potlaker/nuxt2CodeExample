
/** Запрос graphql для топовых игр */
import topUsers from "@/graph/query/topUsers";

export const state = () => ({
    /* массив со списком юзеров */

    userList: [],
    /** номер активной страницы */
    activePage: 1,

    /** общее кол-во элементов(для расчета кол-ва страниц) */
    // TODO рассмотреть возможность получать это значение динамически
    totalItems: 200,

    /** количество элементовкоторые нужно пропустить */
    skip: 0,
  })
  
  export const getters = {
    /** Возвращает номер активной страницы */
    getActivePage(state){
        return state.activePage;
    },

    /** Взвращает общее кол-во элементов для пагинации(расчет максимальной страницы) */
    getTotalItems(state){
        return state.totalItems;
    },

    /** Возвращает список юзеров */
    getUserList(state){
        return state.userList;
    }
  }
  
export const mutations = {
    /** Устанавливает номер активной страницы */
    setActivePage(state, activePage){
        state.activePage = activePage;
    },

    /** Записывает данные о юзерах пришедшие из бд */
    setUsers(state, userList){
        state.userList = userList;
        console.log('state.userList  ==> ', state.userList[0]);
    },

    /** Записывает skip */
    setSkip(state, skip){
        state.skip = skip;
    }
}

  
export const actions = {
    /** Запрашивает данные топ юзеров из бд по graaphql и коммитит в стор */
    async fetchTopUsers({ state, commit }){
        try{
            
            let result = await this.app.apolloProvider.clients.defaultClient.query({
                query: topUsers, 
                variables: {
                    skip: state.skip,
                },
            });
            // console.log('fetchTopUsers ==>', result.data.topUsers[0]);
            // TODO Проверить на ошибки!!!!!!
            commit('setUsers', result.data.topUsers);
        }catch(err){
            console.log('Ошибка в fetchTopUsers ==> ', err)
        }
    },
}
